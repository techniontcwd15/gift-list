const db = require('../db');

class Gift {
  constructor(id, name, url) {
    this.id = id;
    this.name = name;
    this.url = url;
  }

  open(res) {
    res.redirect(this.url);
  }
}

module.exports = {
  getAllGifts() {
    return new Promise((resolve, reject) => {
      db.query('SELECT * FROM `gifts`', (error, results) => {
        if (error) return reject(error);

        resolve(results);
      });
    });
  },

  getGift(id) {
    return new Promise((resolve, reject) => {
      db.query('SELECT * FROM `gifts` WHERE id=?', [id], (error, results) => {
        if (error) return reject(error);

        resolve(new Gift(results[0].id, results[0].name, results[0].url));
      });
    });
  },

  addGift(name, url, cb) {
    db.query('INSERT INTO `gifts` (`name`, `url`) VALUES(?, ?)', [name, url], cb);
  }
}
