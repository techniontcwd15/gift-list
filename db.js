const mysql = require('mysql');

const connection = mysql.createConnection({
  host     : '192.168.120.128',
  user     : 'root',
  password : '1',
  database : 'gift_db'
});

connection.connect((err) => {
  if(err) {
    console.error(err);
    process.exit(1);
  }

  console.log('connected');
});

module.exports = connection;
