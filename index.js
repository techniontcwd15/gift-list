const express = require('express');
const bodyParser = require('body-parser');
const Gifts = require('./models/gifts');

const app = express();

app.use(bodyParser.json());

app.get('/gifts', async (req, res) => {
  const gifts = await Gifts.getAllGifts();

  res.json(gifts);
});

app.get('/gifts/:id', (req, res) => {
  Gifts.getGift(req.params.id).then((gift) =>{
    res.json(gift);
  });
});

app.get('/gifts/:id/open', (req, res) => {
  Gifts.getGift(req.params.id).then((gift) =>{
    gift.open(res);
  });
});

app.post('/gifts', (req, res) => {
  if(!req.body.name || !req.body.url)
    return res.sendStatus(500);

  const gift = req.body;

  Gifts.addGift(gift.name, gift.url, (error) => {
    if(error)
      return res.sendStatus(500);

    res.sendStatus(204);
  });
});

app.listen(8080);
